#include "Vector.h"

/*
* The primary constructor for the Vector class.
* param n: The size of the vector and it's resize factor
* return: None
*/
Vector::Vector(int n)
{
    // If the size of the array is smaller than the minimum, set it as the minimum
    if (n < MIN_SIZE)
    {
	    n = MIN_SIZE;
    }

    // Allocate the array of integers with the size n
    this->_elements = new int[n];

    // Set the capacity and the resize factor of the array as the size of the array
    this->_capacity = n;
    this->_resizeFactor = n;

    // Set the array as empty
    this->_size = 0;
}

/*
* The copy constructor for the Vector class.
* param other: The vector to copy from
* return: None
*/
Vector::Vector(const Vector& other)
{
    // Use the operator = to do deep copy
    *this = other;
}

/*
* The copy operator for the other vector
* param other: The vector to copy from
* return: The reference to the new vector created from the original one
*/
Vector& Vector::operator=(const Vector& other)
{
    // Check if it tries to copy itself and prevent that
    if (this == &other)
    {
        return *this;
    }

    // Copy const fields from the other vector
    this->_size = other._size;
    this->_capacity = other._capacity;
    this->_resizeFactor = other._resizeFactor;

    // Allocate the elements array by the capacity
    this->_elements = new int[this->_capacity];

    // Copy all the values from the other elements array to the new one
    for (int i = 0; i < this->_size; i++)
    {
        this->_elements[i] = other._elements[i];
    }

    // Return the dereference to the current object to allow for middle-result
    return *this;
}

/*
* The destructor for the vector class
* return: None
*/
Vector::~Vector()
{
    // Free the array of elements on destruction
    delete[] this->_elements;
    this->_elements = nullptr;
}

//Getters

int Vector::size() const
{
    return this->_size;
}

int Vector::capacity() const
{
    return this->_capacity;
}

int Vector::resizeFactor() const
{
    return this->_resizeFactor;
}

bool Vector::empty() const
{
    return this->_size == 0;
}

/*
* This function reserves at least n integers in the vector
* param n: The minimum size of the vector
* return: None
*/
void Vector::reserve(int n)
{
    int *newElements = nullptr;

    // Calculate the amount of factors in the delta between the new capacity and the old capacity
    int amountOfFactors = (n - this->_capacity) / this->_resizeFactor;

    // If the new size is bigger than the current capacity
    if (n > this->_capacity)
    {
        // Check if needed 1 more factor to fill the gap that is smaller than the resize factor
        if ((n - this->_capacity) % this->_resizeFactor != 0)
        {
            amountOfFactors++;
        }

        // Set the new capacity of the Vector
        this->_capacity = this->_capacity + amountOfFactors * this->_resizeFactor;

        // Allocate the new array of integers with the new capacity
        newElements = new int[this->_capacity];

        // Copy all the values in the old elements to the new array
        for (int i = 0; i < this->_size; i++)
        {
            newElements[i] = this->_elements[i];
        }

        // Free the old array
        delete[] this->_elements;

        // Set the new array
        this->_elements = newElements;
    }
}

/*
* This function pushes a value to the end of the vector
* param val: The value to push to the vector
* return: None
*/
void Vector::push_back(const int & val)
{
    // If the current elements array is full, allocate more space
    if (this->_size >= this->_capacity)
    {
        // Reserve space in the elements array for more elemnts using 1 resize factor
        this->reserve(this->_capacity + this->_resizeFactor);
    }

    // Set the new value and increase the size of the vector
    this->_elements[this->_size++] = val;
}

/*
* This function pops an integer from the vector
* return: The value or if the vector is empty -9999
*/
int Vector::pop_back()
{
    int value = 0;

    // If the vector is empty
    if (this->empty())
    {
        // Print to the user that vector is empty
        std::cout << "Error: Pop from empty vector!" << std::endl; 
        
        value = VECTOR_EMPTY; 
    } else {
        // Pop the last element in the list and decrease the size of the vector
        value = this->_elements[--this->_size];
    }

    return value;
}

/*
* This function resizes the vector and sets the val in all the new elements
* param n: The new size of the vector
* param val: The value to put in the new elements of the vector
* return: None
*/
void Vector::resize(int n, const int& val)
{
    // If the new size is smaller or equal than the current capacity, only set the size of the vector to it
    if (n <= this->_capacity)
    {
        this->_size = n;
    } else {
        // Re-allocate the array with the new size by the resize factor
        this->reserve(n);

        // Fill in the new indexs the value
        for (int i = this->_size; i < n; i++)
        {
            this->push_back(val);
        }
    }  
}

/*
* This function assigns a value to all the elements in the vector
* param var: The value to assign
* return: None
*/
void Vector::assign(int val)
{
    // Go through the accessible elements in the array
    for (int i = 0; i < this->_size; i++)
    {
        // Set the value of each element to the new value
        this->_elements[i] = val;
    }
}

/*
* This function resizes the vector and sets 0 in all the new elements
* param n: The new size of the vector
* return: None
*/
void Vector::resize(int n)
{
    // Resize the vector using default value of 0
    this->resize(n, 0);
}

/*
* This operator allows direct access to the vector by index
* param n: The index of the element
* return: The reference to the element in the list
*/
int& Vector::operator[](int n) const
{
    // Check if the index is out of the bounds of the vector
    if (n < 0 || n >= this->_size)
    {
        // If it's out of bounds, set the index to be the first value (index 0) and print error
        std::cout << "Error: Index out of bounds!" << std::endl;
        n = 0;
    }

    // Return the element in the n index
    return this->_elements[n];
}

/*
* This operator allow to stream the vector by converting it into string
* param os: The stream
* param vector: The vector to be strem
* return: The reference to the stream
*/
std::ostream& operator<<(std::ostream& os, const Vector& vector)
{
    // Send the basic vector info to the stream
    os << "Vector Info:" << std::endl;
    os << "Capacity is " << vector._capacity << std::endl;
    os << "Size is " << vector._size << std::endl;

    // Send the start of the data array
    os << "Data is {";

    // For each element in the vector, send it to the stream
    for (int i = 0; i < vector._size; i++)
    {
        // If the element isn't the first element, send comma before the number
        if (i != 0)
        {
            os << ", ";
        }

        // Send the value of the current element
        os << vector._elements[i];
    }

    // Send the end of the data
    os << "}" << std::endl;

    return os;
}

/*
* This operator adds a vector to this vector and returns it
* param other: The other vector
* return: The new result vector
*/
Vector Vector::operator+(const Vector& other)
{
    // Create a duplicate of this vector
    Vector dup(*this);

    // Go through the vectors (stop when reaching the limit of one the vectors)
    for (int i = 0; i < this->_size && i < other._size; i++)
    {
        // Add the element from the other vector to the new duplicate vector of this vector
        dup[i] += other[i];
    }

    return dup;
}

/*
* This operator subtracts a vector from this vector and returns it
* param other: The other vector
* return: The new result vector
*/
Vector Vector::operator-(const Vector& other)
{
    // Create a duplicate of this vector
    Vector dup(*this);

    // Go through the vectors (stop when reaching the limit of one the vectors)
    for (int i = 0; i < this->_size && i < other._size; i++)
    {
        // Add the element from the other vector to the new duplicate vector of this vector
        dup[i] -= other[i];
    }

    return dup;
}

/*
* This operator adds a vector to this vector
* param other: The other vector
* return: The reference to this vector
*/
Vector& Vector::operator+=(const Vector& other)
{
    // Use the plus operator with this vector and the other vector and set the result in this vector
    *this = *this + other;

    return *this;
}

/*
* This operator subtracts a vector from this vector
* param other: The other vector
* return: The reference to this vector
*/
Vector& Vector::operator-=(const Vector& other)
{
    // Use the minus operator with this vector and the other vector and set the result in this vector
    *this = *this - other;

    return *this;
}